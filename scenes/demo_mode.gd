
extends Timer

# member variables here, example:
# var a=2
# var b="textvar"
var reset = false

func _ready():
	set_process_input(true)
	set_process(true)
	
func _process(delta):
	if reset == true:
		stop()
		start()
		reset = false

func _input(event):
	if event.is_pressed():
		reset = true

func _on_demo_mode_timeout():
	var game = global.game
	if get_tree().get_current_scene().get_name() != "intro":
		prints("RELOAD")
		if game == "baba":
			get_tree().change_scene("res://scenes/baba/baba_intro.xml")
		elif game == "oyama":
			get_tree().change_scene("res://scenes/karateka_intro.xml")
		elif game == "pio":
			get_tree().change_scene( "res://scenes/pio/pio_intro.xml" )
		elif game == "hira":
			get_tree().change_scene( "res://scenes/hira/hira_intro.xml" )
