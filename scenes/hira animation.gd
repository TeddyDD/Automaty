
extends Node2D

var lock = true

func _ready():
	set_process(true)
	
func _process(delta):
	if not lock:
		if Input.is_action_pressed("ui_accept"):
			next_level()

func next_level():
	get_tree().change_scene("res://scenes/hira.xml")

func _on_lock_timeout():
	lock = false
