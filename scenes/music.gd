
extends Node

var _children = []
var now = ""

func _ready():
	_children = get_children()

func stop_all():
	for c in _children:
		c.stop()
		
func is_playing():
	for c in _children:
		if c.is_playing():
			return true
	return false

func play(what=null):
	if what != null:
		now = what
	get_node(str(now)).play()