
extends Timer

# member variables here, example:
# var a=2
# var b="textvar"
var reset = false

func _ready():
	set_process_input(true)
	set_process(true)
	
func _process(delta):
	if reset == true:
		stop()
		start()
		reset = false

func _input(event):
	if event.is_pressed():
		reset = true

func _on_music_mute_timeout():
	music.stop_all()
	prints("MUTE")