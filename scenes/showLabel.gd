
extends Label

onready var timer = get_node("Timer")
var __obj
var __method
var __hide

func _ready():
	hide()
	timer.connect("timeout", self, "timeout")

func show_and_hide(time, text):
	set_text(text)
	__hide = true
	show()
	timer.set_wait_time(time)
	timer.start()

func show_then_do(time, obj, method, hide_after=true):
	show()
	timer.set_wait_time(time)
	timer.start()
	__obj = obj
	__method = method
	__hide = hide_after
	prints(timer.get_time_left())
	
func timeout():
	if __hide:
		hide()
	if __obj != null:
		__obj.call(__method)