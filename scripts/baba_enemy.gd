
extends Area2D

# state
export var direction = "right" setget direction_set
var happy = false
var game_over = false
var stop = false
var road

# nodes
onready var level = get_parent()

onready var animation = get_node("AnimationPlayer")
export var speed = 20


func _ready():
	set_fixed_process( true )
	speed = speed - 10 + randi() % 20
	# scale and flip
	var f = 1
	if direction == "left":
		f = -1
	if road == 3:
		set_scale(Vector2(1*f,1))
		set_z( 7 ) # pilars = 6
	elif road == 2:
		set_scale(Vector2(0.9*f,0.9))
		set_z( 4 ) # pilars = 3
	else:
		set_scale(Vector2(0.8*f,0.8))
		set_z( 1 ) # pilars 2
func _fixed_process(delta):
	var current_animation = animation.get_current_animation()
	var new_animation =  current_animation
	
	var motion = Vector2()
	if not stop and not happy and not level.game_over:
		if direction == "right":
			motion = Vector2(speed,0)
		else:
			motion = Vector2(-speed, 0)
		set_pos(get_pos() + (motion * delta))
		new_animation = "move"
	if happy:
		if direction == "right":
			motion = Vector2(-speed,0)
		else:
			motion = Vector2(speed,0)
		set_pos(get_pos() + (motion * delta))
	
	# animation
	if stop:
		new_animation = "idle"
	if level.game_over and not happy:
		new_animation = "idle"
	if happy:
		new_animation = "happy"
		
	if not new_animation == current_animation:
		animation.play(new_animation)
		
	# destroy out of screen
	if get_pos().x > 340 and happy or get_pos().x < -10 and happy: # out of screeen
		get_parent().enemies_left -= 1
		queue_free()

func game_over():
	game_over = true


func _on_enemy_area_enter( area ):
	if area.get_pos().x > get_pos().x:
		stop = true
		get_node("stop_timer").start()


func _on_stop_timer_timeout():
	stop = false

# hit by necklace
func _on_enemy_body_enter( body ):
	if body extends preload("res://scripts/necklace.gd") and not happy:
		get_node("SamplePlayer2D").play("catch")
		happy = true
		speed += 40
		# go up
		if road == 3:
			set_pos( Vector2( get_pos().x, get_pos().y - 10) )
		elif road == 2:
			set_pos( Vector2( get_pos().x, get_pos().y - 9) )
		else:
			set_pos( Vector2( get_pos().x, get_pos().y - 8) )
		set_z( get_z() - 1 )
		body.queue_free()
		global.score += 10
		
func direction_set(new_value):
	if new_value == "left":
		set_scale( Vector2(-1,1) )
	else:
		set_scale( Vector2(1,1) )
	direction = new_value
