
extends Node2D

# spawner controls

var MAX_SPAWN = 3
var SPAWN_CAP = 8
var timeouts = 0

# minigame
var level = 1
var minigame = false
var pre_minigame = false
var pressed_prev = false
var pressed_now = false
var minigame_progress = 0
var minigame_final = 100.0
var enemies_left = 10
var enemies_left_prev = 10
var already_won = false

# state
var game_over = false

# levels
var level_bg = -1
var level_bgs = [
                 preload("res://textures/baba scena nowa.png"),
                 preload("res://textures/baba scena nowa drzewo level2.png"),
                 preload("res://textures/baba level 3.png"),
                ]

var enemy_textures = [
                      preload("res://textures/dziad-Sheet.png"),
                      preload("res://textures/man-Sheet.png"),
                      preload("res://textures/woman-Sheet.png"),
                     ]

func _ready():
	global.score = 0
	global.game = "baba"
	get_node("gui/points").set_text(str(global.score))
	get_node("gui/level").set_text( "LVL: " + str(level))
	music.get_node("tapper").play()
	randomize()
	get_node("minigame_time_remaining").set_wait_time(max(3, 10 - level))
	set_fixed_process(true)
	next_bg()

	
func _fixed_process(delta):
	get_node("gui/points").set_text(str(global.score))
	
	if minigame or pre_minigame:
		clean_level()
	
	if minigame:
		
		pressed_now = Input.is_action_pressed("ui_accept")
		
		if pressed_now and not pressed_prev: # press
			minigame_progress += 200 * delta
		elif pressed_prev and not pressed_now: # release
			minigame_progress += 200 * delta
		elif pressed_now and pressed_prev: # hold
			pass 
		else: # idle
			if minigame_progress > 0:
				minigame_progress -= 45 * delta
		
		pressed_prev = pressed_now
		var t = get_node("minigame_time_remaining")
		get_node("minigame/bar").set_value( lerp(0, 13, minigame_progress/minigame_final))
		get_node("minigame/time_bar").set_value( lerp(0, 13, t.get_time_left()/t.get_wait_time()))
		get_node("minigame/baba").set_frame( lerp(0, 6, minigame_progress/minigame_final))
		
		if minigame_progress >= 100 and not already_won:
			minigame_won()
	elif enemies_left <= 0 and not minigame:
		if not pre_minigame and level % 2 == 0:
			pre_minigame = true
			get_node("player").lock = true
			clean_level()
			get_node("hud/bonus stage").show_then_do(2, self, "show_minigame")
	#		show_minigame()
		elif not pre_minigame:
			enemies_left = enemies_left_prev + level
			enemies_left_prev = enemies_left
			pre_minigame = true
			next_level()

	
func clean_level():
	for e in get_tree().get_nodes_in_group("enemy"):
		e.queue_free()
	for e in get_tree().get_nodes_in_group("bullet"):
		e.queue_free()
	
func minigame_won():
	already_won = true
	global.score += 100
	get_node("minigame/SamplePlayer2D").play("catch")
	get_node("minigame/AnimResult").play("won")

func minigame_lost():
	minigame_progress = 0
	get_node("minigame/AnimResult").play("lost")
	
func show_minigame():
	pre_minigame = false
	already_won = false
	get_node("level_time_remaining").stop()
	for e in get_tree().get_nodes_in_group("enemy"):
		e.queue_free()
	minigame = true
	
	# cleanup
	get_node("minigame/won").hide()
	get_node("minigame/baba").set_animation("default")
	
	get_node("minigame").show()
	get_node("minigame_time_remaining").start()
	
	# music
	music.stop_all()
	music.play("baba_minigame")

# called from animation
func hide_minigame():
	minigame = false
	get_node("player").start_lock()
	get_node("minigame_time_remaining").stop()
	get_node("minigame").hide()
	minigame_progress = 0
	get_node("minigame/bar").set_value(0)
	enemies_left = enemies_left_prev + level
	enemies_left_prev = enemies_left
	next_level()
	get_node("level_time_remaining").start()
	get_node("player").lock = false
	# music
	music.stop_all()
	music.play("tapper")

func next_level():
	level += 1
	next_bg()
	clean_level()
	pre_minigame = true
#	get_node("hud/level prompt").show_and_hide(2, "Level %d" % level)
	get_node("hud/level prompt").set_text("Level %d" % level)
	get_node("hud/level prompt").show_then_do(2, self, "level_prompt")


func level_prompt():
		get_node("hud/level prompt").hide()
		pre_minigame = false


func spawn_enemy(spawn):
	if get_tree().get_nodes_in_group("enemy").size() < SPAWN_CAP:
		assert( spawn >= 1 and spawn <= 6)
		var enemy = preload("res://scenes/baba/enemy.xml").instance()
		enemy.set_pos( get_node("spawns/spawn" + str(spawn)).get_pos() )
		
		# randomiz pos a bit
		enemy.set_pos(enemy.get_pos() + Vector2( -10 + randi() % 20, 0))
		
		if spawn <= 3:
			enemy.direction = "right"
		else:
			enemy.direction = "left"
		# scale
		if spawn == 1 or spawn == 4:
			enemy.road = 1
		elif spawn == 2 or spawn == 5:
			enemy.road = 2
		elif spawn == 3 or spawn == 6:
			enemy.road = 3
		
		# texture
		var t = randi() % enemy_textures.size()
		enemy.get_node("Sprite").set_texture( enemy_textures[t] )
		
		add_child( enemy )
	
# game over

func _on_crapet_area_enter( area ):
#	area.game_over()
	set_game_over()

func set_game_over():
	if not game_over:
		game_over = true
		get_node("hud/game_over").show()
		get_node("hud/score").set_text("Points: " + str(global.score))
		get_node("hud/score").show()
		
		get_node("level_time_remaining").stop()
		get_node("minigame_time_remaining").stop()
	

# spawn enemies
func _on_spawn_timer_timeout():
	if not game_over and not minigame:
		var how_many = randi() % MAX_SPAWN # 0 to MAX_SPAWN
		var roads = pick_roads(how_many)
		for r in roads:
			spawn_enemy(r)
		
		timeouts += 1
		
		# increase ammount of enemies
		var t = get_node("spawn_timer")
		if timeouts % 3 == 0 and t.get_wait_time() > 0.55:
			t.set_wait_time(t.get_wait_time() - 0.40)
			if MAX_SPAWN < 6 and randi() % 10 < 3:
				MAX_SPAWN += 1
		
# select random roads
func pick_roads(how_many):
	var roads = []
	while roads.size() < how_many:
		var i = (randi() % 6) + 1
#		if roads.find(i) == -1:
		roads.append(i)
	return roads
	
# called from player
func reset_game():
	for e in get_tree().get_nodes_in_group("enemy"):
		e.queue_free()
	get_node("hud/game_over").hide()
	get_node("hud/score").hide()
	global.score = 0
	game_over = false

func _on_level_time_remaining_timeout():
	get_node("gui/level").set_text( "LVL: " + str(level))


func _on_minigame_time_remaining_timeout():
	minigame_lost()
	
func next_bg():
	for p in get_tree().get_nodes_in_group("pilars"):
		p.hide()
	if level_bg < level_bgs.size() - 1:
		level_bg += 1
	else:
		level_bg = 0
	get_node("bg").set_texture( level_bgs[level_bg] )
	get_node("pillars%d" % level_bg).show()