
extends Node2D

# state
var prev_road = 2
var jumping = false
var throwing = false
var road = 2           # 1 - top, 3 - down
var direction = "left" # which direction facing

var lock = true

# nodes
var sprite
var animation

func _ready():
	sprite = get_node("Sprite")
	animation = get_node("AnimationPlayer")
	set_fixed_process( true )
	start_lock()
	
func start_lock():
	lock = true
	get_node("unlock").start()
	
func _fixed_process(delta):
	prev_road = road
	
	# input

	var up = Input.is_action_pressed("ui_up")
	var down = Input.is_action_pressed("ui_down")
	var left = Input.is_action_pressed("ui_left")
	var rigt = Input.is_action_pressed("ui_right")
	var throw = Input.is_action_pressed("ui_accept")
	var reset_action = Input.is_action_pressed("ui_accept")
	
	var wanted_road
	
	
	# process input
	if not get_parent().game_over and not get_parent().minigame:
		if left:
			direction = "left"
		elif rigt:
			direction = "right"
		else:
			direction = direction
		if not jumping:
			if up and not road == 1:
				jumping = true
				road -= 1
			elif down and not road == 3:
				jumping = true
				road += 1
		if not jumping and not throwing and throw and not lock:
			throwing = true
		
		
		# update sprite
		if direction == "right": # default is left
			sprite.set_flip_h( true )
		else:
			sprite.set_flip_h( false )
		
		# scale
		if road == 3:
			set_scale(Vector2(1,1))
		elif road == 2:
			set_scale(Vector2(0.9,0.9))
		else:
			set_scale(Vector2(0.8,0.8))
	
	if not get_parent().minigame:
		if jumping:
			throw = false
			throwing = false
			if not animation.get_current_animation() == "move":
				animation.play("move")
		elif throwing and not lock:
			if not animation.get_current_animation() == "throw" and not lock:
				animation.play("throw")
				get_node("SamplePlayer2D").play("throw")
		elif reset_action and not lock:
			get_tree().change_scene("res://scenes/hiscore.tscn")
#		get_parent().reset_game()
# called from animation

func jump():
	set_pos( get_node("../spot"+str(road)).get_pos() )
		
func reset_jump():
	jumping = false
	animation.play("idle")
	
func spawn_necklace():
	var necklace = preload("res://scenes/baba/necklace.tscn").instance()
	if direction == "right":
		necklace.set_pos( Vector2(get_pos().x + 12, get_pos().y + 5 ) )
	else:
		necklace.set_pos( Vector2(get_pos().x - 12, get_pos().y + 5 ) )
	necklace.direction = direction
	get_parent().add_child( necklace )
	
func reset_throwing():
	throwing = false 
	animation.play("idle")

func _on_unlock_timeout():
	lock = false
