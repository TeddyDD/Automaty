
extends Node2D

# spawner controls

var MAX_SPAWN = 3 
var SPAWN_CAP = 8
var timeouts = 0

var left = 0
var right = 0

var conf = false

func _ready():
	randomize()
	set_fixed_process( true )
	
func _fixed_process(delta):
	if not conf:
		var w = get_tree().get_nodes_in_group( "waiting" )
#		prints("Waiting", w)
		if w.size() > 0:
			w[randi() % w.size()].start_confession()
			conf = true
	
func spawn_enemy(spawn):
	if get_tree().get_nodes_in_group("enemy").size() < SPAWN_CAP:
		assert( spawn == "r" or spawn == "l")
		var enemy = preload("res://scenes/pio/prayer.xml").instance()
		enemy.set_pos( get_node("spawn_" + str(spawn)).get_pos() )
		
		# randomiz pos a bit
#		enemy.set_pos(enemy.get_pos() + Vector2( -10 + randi() % 20, 0))
		
		if spawn == "r":
			enemy.direction = "left"
			right += 1
		else:
			enemy.direction = "right"
			left += 1
		add_child( enemy )
		prints("spawn")
		
func auto_spawn():
	if left > right:
		spawn_enemy("r")
	elif right > left:
		spawn_enemy("l")
	else:
		spawn_enemy("r")

func _on_spawn_timer_timeout():
	auto_spawn()
	auto_spawn()
