
extends Node2D

var wanted_pos
var stop
var speed = 5

var fire_speed = 100
var fireball = preload("res://scenes/pio/fireball.tscn")
var max_fireballs = 2
var spawn_cooldown = false

func _ready():
	wanted_pos = get_pos()
	set_fixed_process( true )
	
func _fixed_process(delta):
	if get_parent().play:
		var move = Vector2()
		if wanted_pos.x > get_pos().x: # right
			move.x = speed * delta
			get_node("Sprite").set_flip_h(true)
		else: # left
			move.x = -speed * delta
			get_node("Sprite").set_flip_h(false)
		if not abs( get_pos().x - wanted_pos.x ) < 5:
			set_pos(get_pos() + move)
		else: # on place
			wanted_pos = get_pos()
			if get_node("../YSort/pio").get_pos().x > get_pos().x: #right
				get_node("Sprite").set_flip_h(true)
			elif get_node("../YSort/pio").get_pos().x < get_pos().x: #left
				get_node("Sprite").set_flip_h(false)
			auto_spawn()


func auto_spawn():
	if get_parent().play:
		if get_pos().x > get_node("../YSort/pio").get_pos().x: # left
			get_node("Sprite").set_flip_h(false)
			spawn_fireball("left")
		else: # right
			get_node("Sprite").set_flip_h(true)
			spawn_fireball("right")

func spawn_fireball(dir):
	prints("prespawn")
	if get_tree().get_nodes_in_group("fireball").size() < max_fireballs and not spawn_cooldown:
		var i = fireball.instance()
		i.set_pos(get_pos())
		if dir == "left":
			i.direction = "left"
		else:
			i.direction = "right"
		get_parent().get_node("YSort").add_child(i)
		spawn_cooldown = true
		get_node("spawn_cooldown").start()
		prints("postspawn")
	
	
func pick_spot():
	var r = (randi() % 300) + 20
	wanted_pos = Vector2(r, get_pos().y)

func _on_change_pos_timeout():
	pick_spot()
	prints("pick ", wanted_pos)


func _on_spawn_cooldown_timeout():
	spawn_cooldown = false


func _on_auto_spawn_timeout():
	auto_spawn()
