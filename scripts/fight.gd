
extends Node2D

var play = false # for testing

func _ready():
	pass

# called from animation

func start():
	play = true

func stop():
	play = false
	var fire = get_tree().get_nodes_in_group("fireball")
	for f in fire:
		f.queue_free()