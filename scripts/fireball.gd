
extends Area2D

var rot_speed = 60
onready var sprite = get_node("Sprite")
var speed = 80
var direction = "left" setget direction_set
var motion = Vector2()

func _ready():
	set_fixed_process(true)
	randomize()
	
func direction_set(value):
	if value == "left":
		prints("side left")
		motion = Vector2(-speed + (randi() % 10 + 25),speed)
	else:
		prints("side right")
		motion = Vector2(speed - (randi() % 80 + 25),speed)
	direction = value
	
func _fixed_process(delta):
	set_pos(get_pos() + motion * delta)
	if get_pos().y < 29:
		motion.y = -motion.y 
#		motion.x += (randi() % 20) - 10 * delta
	elif get_pos().y > 114:
		motion.y = -motion.y 
#		motion.x += (randi() % 20) - 10 * delta
	if get_pos().x > 330 or get_pos().x < -10 and get_parent().get_parent().play:
		global.score += 50
		queue_free()
	


