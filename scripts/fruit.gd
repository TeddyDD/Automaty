
extends Area2D

# member variables here, example:
# var a=2
# var b="textvar"

var taken = false

func _ready():
	pass

func _on_appple_body_enter( body ):
	if not taken and body extends preload("res://scripts/player_hira.gd"): 
		taken = true
		if global.score > 2:
			global.score -= 2
		else:
			global.score = 0
		get_node("AnimationPlayer").play("taken")
		get_tree().get_current_scene().get_node("player/SamplePlayer2D").play("food")
		get_tree().get_current_scene().get_node("player").stopped = true
#		get_tree().get_current_scene().get_node("player").new_anim = "fruit"
		get_tree().get_current_scene().get_node("player/fruit_timer").start()