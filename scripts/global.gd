
extends Node

var scores = {
	baba  = [],
	oyama = [],
	pio   = [],
	hira  = []
	}

var score = 0
var game

const score_file = "user://score.json"

func _ready():
	set_process(true)
	var f = File.new()
	if not f.file_exists(score_file):
		save_scores()
	else:
		scores = load_scores()
	prints("Score is %s" % scores)
	
#	## DEBUG
#	add_score(100, "TEDDY", "hira")
#	save_scores()
#	
#	prints("BEST SCORES")
#	prints(get_high_scores("hira", 3))
	
func _process(delta):
	if Input.is_action_pressed("click"):
		get_tree().quit()

func add_score(points, name, game):
	var p = [points, name]
	scores[game].push_back(p)
	
func load_scores():
	var f = File.new()
	f.open(score_file, f.READ)
	var j = f.get_as_text()
	f.close()
	
	var result = {}
	result.parse_json(j)
	return result

func save_scores():
	var f = File.new()
	f.open(score_file, f.WRITE)
	f.store_string(scores.to_json())
	f.close()
	
func get_high_scores(game, count):
	var s = scores[game]
	s.sort_custom(Sorter, "is_smaller")
	s.invert()
	var result = []
	for i in range(count):
		result.append(s[i])
	return result
	

class Sorter:
	# return true if the first argument is less than the second
	static func is_smaller(a, b):
		if a[0] < b[0]:
			return true
		else:
			return false