
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"

var score = 0
var lives = 3
var time_left = 0
var TIME_LEVEL = 60

var levels = [
              preload("res://scenes/hira/level0.xml"),
              preload("res://scenes/hira/level5.xml"),
              preload("res://scenes/hira/level2.xml"),
              preload("res://scenes/hira/level3.xml"),
              preload("res://scenes/hira/level4.xml"),
             ]
var current_level = 0

# camera
onready var camera = get_node("Camera")
export(float) var cam_speed = 40
var scroll = false


var prev_jump_pressed = false
#onready var frame = get_node("frame")

# progress, sky
var level_progress = 0
var sky_pos_y = lerp(41.220299, 197.370422, 0)
onready var level_to_pass = camera.get_pos().distance_to(get_node("level/end_level").get_pos())
onready var bg = get_node("bg/background")
var timer = 0
var timer_active = true

onready var player = get_node("player")

func _ready():
	global.game = "hira"
	current_level = 0
	
	load_level(current_level)
	
	set_process( true )


func load_level(index):
	music.stop_all()
	music.play("hira")

	camera.make_current()
	var c = get_node("level")
	if not c == null:
		c.queue_free()
		remove_child( c )
	prints("Load level " + str(index))
	var level = levels[index].instance()
	add_child(level)
	player.set_pos(get_node("level").get_pos())
	player.level_over = false
	current_level = index
	camera.set_pos(player.get_pos())
	
	player.level_over = false
	player.game_over = false
	player.camera_follow = true
	get_node("hud/game_over").hide()
	get_node("hud/end_screen").show()
	get_node("hud/end_screen/picture").hide()
	show_lives_screen()
	timer = 0
	timer_active = true
	
	time_left = TIME_LEVEL
	get_node("hud/end_screen/Time").set_text("Time: %d" % time_left)

func _process(delta):
#	prints(player.get_pos().x )
	if timer_active:
		timer += delta
	if scroll:
		camera.set_pos( camera.get_pos() + Vector2(cam_speed,0) * delta ) # x
	var cam_diff = abs(camera.get_pos().y - player.get_pos().y)
	
	if player.camera_follow:
		var wanted_y = player.get_pos().y
		camera.set_pos( Vector2(camera.get_pos().x , wanted_y)) # y
#	frame.set_global_pos( (frame.get_global_pos() + Vector2(-160,-120)) + Vector2(cam_speed,0) * 
	# RESTART
	
	var jump = Input.is_action_pressed("ui_accept")
	if not jump:
		jump = Input.is_action_pressed("ui_cancel")
	var jmp_state
	if prev_jump_pressed and jump: 
		jmp_state = "h"
	elif not prev_jump_pressed and jump:
		jmp_state = "p"
	elif prev_jump_pressed and not jump:
		jmp_state = "r"
	else:
		jmp_state = "n"
	prev_jump_pressed = jump
	
	if player.game_over and time_left <= 0:
		get_node("hud/game_over").set_text("Time is over")
		get_node("hud/game_over").show()
	
	
	if player.game_over and jmp_state == "p" and lives > 1:
		player.game_over = false
		lives -= 1
		get_node("hud/game_over").hide()
		load_level(current_level)
	elif player.game_over and lives <= 1:
		lives = 0 
		get_node("hud/game_over").set_text("GAME OVER")
		if jmp_state == "p":
			get_tree().change_scene("res://scenes/hiscore.tscn")
	
	# NEXT LEVEL
	if player.level_over and jmp_state == "p":
		player.level_over = false
		get_node("hud/end_screen").hide()
		if current_level < levels.size() - 1:
			load_level(current_level + 1)
			global.score += 5
		else:
			if jmp_state == "p":
				global.score += 5
				get_tree().change_scene("res://scenes/hiscore.tscn")
			
	# counters
	var time = str(round(timer))
	var points  = get_node("hud/end_screen/points")
	points.set_text( str( global.score ) )
	get_node("hud/end_screen/level").set_text("Level: %s" % str(current_level + 1))
	get_node("hud/end_screen/Lives").set_text("Lives %d" % lives)
	get_node("hud/end_screen/Time").set_text("Time: %d" % time_left)	
			
			
	# SKY MOVEMENT
	var level_passed = camera.get_pos().distance_to(get_node("level/end_level").get_pos())
	level_progress = level_passed / level_to_pass
	sky_pos_y = lerp(197.370422, 41.220299, level_progress)
	bg.set_pos( Vector2(bg.get_pos().x, sky_pos_y) )
	
# END LEVEL
func on_level_end(body):
	if body extends preload("res://scripts/player_hira.gd"):
		get_node("second").stop()
		timer_active = false
		global.score += floor(time_left/3)
		
		player.stopped = true
		player.level_over = true
		player.get_node("Camera2D").make_current()
		get_node("over").start()
		
func _on_over_timeout():
	player.stopped = false
	next_level()
	player.level_over = true
	
func next_level():
	get_node("second").stop()
	get_node("hud/end_screen/picture").show()
# DROWN
func on_dead(body):
	if body extends preload("res://scripts/player_hira.gd"):
		get_node("player").game_over = true
		get_node("hud/game_over").show()
		get_node("second").start()			
		player.camera_follow = false

func _on_scroll_body_enter( body ):
	if body extends preload("res://scripts/player_hira.gd"):
		scroll = true
		cam_speed = 80

func _on_scroll_body_exit( body ):
	if body extends preload("res://scripts/player_hira.gd"):
		scroll = false

func _on_scroll_faster_body_enter( body ):
	if body extends preload("res://scripts/player_hira.gd"):
		scroll = true
		cam_speed = 160


func _on_scroll_faster_body_exit( body ):
	if body extends preload("res://scripts/player_hira.gd"):
		scroll = false


func show_lives_screen():
	get_node("second").stop()	
	var h = get_node("hud/lives_screen")
	h.get_node("lives").set_text("X %d" % lives)
	h.get_node("lives_hide").start()
	player.stopped = true
	h.show()
	
func _on_lives_hide_timeout():
	get_node("hud/lives_screen").hide()
	player.stopped = false
	get_node("second").start()
	

func _on_second_timeout():
	if time_left == 0:
		get_node("second").stop()
		player.game_over = true
	else:
		time_left -= 1
