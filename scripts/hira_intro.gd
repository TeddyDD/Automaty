
extends Node2D

var lock = true

func _ready():
	set_process( true )
	global.score = 0
	music.stop_all()
	music.play("hira")
	
func _process(delta):
	if not lock:
		if Input.is_action_pressed("ui_accept") or Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left"):
			get_tree().change_scene( "res://scenes/hira animation.tscn" )

func _on_lock_timeout():
	lock = false
