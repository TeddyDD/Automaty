
extends Node2D

# score info
var game = global.game
var points = global.score
var name # <= input


var current_points = 0

var letters = []
# yeah, really
var alphabet = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 1 2 3 4 5 6 7 8 9 0".split(" ")
var current_pos = 0
var current_char = 0

var color_select = Color("#f1ff00")
var color_normal = Color("#ffffff")

var lock = true
var input_state = true

func array_circle(array, value):
	if value > array.size() - 1:
		return 0
	elif value < 0:
		return array.size() - 1
	else: return value
	
func _ready():
	alphabet.append(" ")
	for i in range(1,7):
		letters.append(get_node("input/VBoxContainer/edit/%d" % i))
		letters[-1].set("custom_colors/font_color", color_normal)
	#get_node("input/VBoxContainer/points").set_text(str(points))
	get_node("input/VBoxContainer/points").set_text(str(0))
	redraw()
	set_fixed_process(true)
	
func _fixed_process(delta):
	var up = Input.is_action_pressed("ui_up")
	var down = Input.is_action_pressed("ui_down")
	var left = Input.is_action_pressed("ui_left")
	var right = Input.is_action_pressed("ui_right")
	var done = Input.is_action_pressed("ui_accept")
	
	if game == "pio":
		if Input.is_action_pressed("ui_cancel"):
			done = true
	elif game == "oyama":
		
		up = Input.is_action_pressed("ui_accept")
		right = Input.is_action_pressed("ui_cancel")
		done = Input.is_action_pressed("ui_alternative")
	
	# 0 at the begiinig
	if current_points < points:
		current_points += points / 100.0 
		get_node("input/VBoxContainer/points").set_text(str(int(current_points)))
	else:
		get_node("input/VBoxContainer/points").set_text(str(points))
	
	if get_node("list").is_visible() and Input.is_action_pressed("ui_cancel") and not lock:
		# restart game
		if game == "baba":
			get_tree().change_scene("res://scenes/baba/baba_intro.xml")
		elif game == "oyama":
			get_tree().change_scene("res://scenes/karateka_intro.xml")
		elif game == "pio":
			get_tree().change_scene( "res://scenes/pio/pio_intro.xml" )
		elif game == "hira":
			get_tree().change_scene( "res://scenes/hira/hira_intro.xml" )
			
	if not lock and input_state:
		if done:
			lock = true
			get_node("Timer").start()
			switch_to_list()
		if up or down or left or right:
			if up:
				current_char = array_circle(alphabet, current_char + 1)
			elif down:
				current_char = array_circle(alphabet, current_char - 1)
			else:
				if right:
					current_pos = array_circle(letters, current_pos + 1)
				elif left:
					current_pos = array_circle(letters, current_pos - 1)
				current_char = Array(alphabet).find(letters[current_pos].get_text())
			lock = true
			get_node("Timer").start()
			
	redraw()
	
func redraw():
	for l in range(letters.size()):
		letters[l].set("custom_colors/font_color", color_normal)
	letters[current_pos].set("custom_colors/font_color", color_select)
	letters[current_pos].set_text(alphabet[current_char])

func switch_to_list():
	prints("SWITCH")
	input_state = false
	get_node("input").hide()
	# collect name
	for i in range(letters.size()):
		name = "%s%s" % [name, letters[i].get_text()]
	prints("NAME IS %s" % name)
	
	# save
	global.add_score( points, name, game)
	global.save_scores()
	
	var z = global.scores[game].size()
	var best = global.get_high_scores( game, min(6,z))
	for b in range(0,min(6,z)):
		var label = get_node("list/VBoxContainer/%s" % str(b + 1))
		label.set_text("%d %s" % [best[b][0], best[b][1]])
	get_node("list").show()

func _on_Timer_timeout():
	lock = false
