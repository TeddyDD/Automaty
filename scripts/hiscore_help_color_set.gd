
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"

func _ready():
	if global.game == "pio":
		var b = get_node("button_hint")
		b.set_modulate( Color(0, 0.55, 1) )
		b.set_texture( preload("res://textures/help/szary guzik.png") )
	elif global.game == "oyama":
		hide()
		get_parent().get_node("help_oyama").show()


