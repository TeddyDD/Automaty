
extends Node2D

var max_enemies = 3
var max_bulls = 2

var enemy
var bull
var oyama

var time = 0

func _ready():
	set_process( true )
	global.game = "oyama"
	enemy = load("res://scenes/karateka/enemy.xml")
	bull = load("res://scenes/karateka/bull.xml")
	oyama = get_node("oyama")
	global.score = 0
	
	
func _process(delta):
	get_node("points_counter").set_text( "Points: " + str(global.score))
	get_node("time").set_text( "Time: " + str(time))
	
	
	var enemies = get_tree().get_nodes_in_group("enemy")	
	
	if enemies.size() < max_enemies:
#		for i in range(max_enemies - enemies.size()):
#			if randi() % 4 > 2:
		var addEnemy = enemy.instance()
		addEnemy.set_pos(Vector2(337,165))
		get_tree().get_current_scene().add_child(addEnemy)

#	if bulls.size() < max_bulls and not oyama.died:
#		for i in range(max_bulls - bulls.size()):
#			if randi() % 10 > 3:
#				var addBull = bull.instance()
#				addBull.set_pos(Vector2(350,163))
#				get_tree().get_current_scene().add_child(addBull)
	if oyama.died: # i hope this won't kill performance
		get_node("game_over").show()
		get_node("points").set_text("Score: " + str(global.score))
		get_node("points").show()
		if Input.is_action_pressed("ui_accept"):
			# CHANGE TO HISCORE
			get_tree().change_scene( "res://scenes/hiscore.tscn" )
			

func _on_Timer_timeout():
	if not max_bulls > 3:
		max_bulls += 1
	if not max_enemies > 5:
		max_enemies += 1


func _on_time_counter_timeout():
	time += 1


func _on_bull_spawner_timeout():
	var bulls = get_tree().get_nodes_in_group("bull")
	if bulls.size() < max_bulls:
		var addBull = bull.instance()
		addBull.set_pos(Vector2(350,163))
		get_tree().get_current_scene().add_child(addBull)
