
extends KinematicBody2D

export var speed = 40
var attack = false
var oyama
export var is_bull = false

var animation

func _ready():
	animation = get_node("AnimationPlayer")
	animation.play("move")
	oyama = get_node("../oyama")
	
	set_process( true )
	oyama.connect("on_hit", self, "on_dead") # connect signals
	
	#randomize speed a bit
	speed = speed - 15 + randi() % 30
	
	#random alternative texture
	if randi() % 10 < 3 and not is_bull:
		get_node("Sprite").set_texture( preload("res://textures/przeciwnik2-Sheet.png") )
	
func kill_oyama():
	if not is_bull:
		set_process(false)
	else:
		animation.play("move")
	oyama.kill() # clean this

func on_dead(type):
	if not oyama.died:
		if type == "hit" and get_pos().distance_to(oyama.get_pos()) < 40 and not animation.get_current_animation() == "die" and not is_bull:
			set_process(false)
			animation.play("die")
			global.score += 10
		if type == "kick" and get_pos().distance_to(oyama.get_pos()) < 60 and not animation.get_current_animation() == "die" and not is_bull:
			set_process(false)
			animation.play("die")
			global.score += 10
		if type == "kick" and get_pos().distance_to(oyama.get_pos()) < 70 and not animation.get_current_animation() == "die": # bull
			set_process(false)
			animation.play("die")
			global.score += 50
func _process(delta):
	var postion = Vector2(-speed,0) * delta
	if get_pos().distance_to(oyama.get_pos()) < 45 - randi() % 15 and not attack:
		attack = true
		animation.play("attack")
		set_pos( get_pos() + postion )
	elif oyama.died == true and not is_bull:
		animation.play("idle")
		set_process(false)
	else:
		move( postion )
