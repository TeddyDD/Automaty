
extends TextureFrame

var lock = true

func _ready():
	set_process( true )
	music.get_node("oyama_title").stop()
	music.get_node("oyama").stop()
	music.get_node("oyama_title").play()
	
func _process(delta):
	if not lock:
		var i = Input.is_action_pressed("ui_accept")
		if i:
			get_tree().change_scene( "res://scenes/karateka/cutscene.xml" )

func _on_Timer_timeout():
	lock = false
