
extends Node2D

var pointer
var item = 1
var changing = false

func _ready():
	pointer = get_node("pointer")
	pointer.set_pos(get_node(str(item)).get_pos())
	set_process(true)
	set_process_input(true)
	
func _input(event):
	prints(event)

func _process(delta):
	if not changing:
		var up = Input.is_action_pressed("ui_up")
		var down = Input.is_action_pressed("ui_down")
		if up or down:
			changing = true
		if changing:
			if up and item > 1:
				item -= 1
			elif down and item < 4:
				item += 1
			get_node("AnimationPlayer").play("switch")
	if Input.is_action_pressed("ui_accept"):
		#Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		if item == 1:
			_on_b_baba_pressed()
		elif item == 2:
			_on_b_hira_pressed()
		elif item == 3:
			_on_b_pio_pressed()
		else:
			_on_b_karateka_pressed()
		
		
func change():
	pointer.set_pos(get_node(str(item)).get_pos())
	changing = false
	
func _on_b_karateka_pressed():
#	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().change_scene( "res://scenes/karateka_intro.xml" )


func _on_b_hira_pressed():
#	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().change_scene( "res://scenes/hira/hira_intro.xml" )


func _on_b_baba_pressed():
#	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().change_scene( "res://scenes/baba/baba_intro.xml" )


func _on_b_pio_pressed():
#	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().change_scene( "res://scenes/pio/pio_intro.xml" )
