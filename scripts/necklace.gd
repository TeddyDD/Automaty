
extends KinematicBody2D

var rot_speed = 11
var throw_speed = 100
var direction = ""
onready var sprite = get_node("Sprite")

func _ready():
	set_fixed_process( true )
	get_node("AnimationPlayer").play("spin")
	
func _fixed_process(delta):
	
	if direction == "right":
		sprite.set_rot( sprite.get_rot() + (-rot_speed * delta) )
		set_pos( Vector2( get_pos().x + (throw_speed * delta), get_pos().y ) )
	else:
		sprite.set_rot( sprite.get_rot() + (rot_speed * delta) )
		set_pos( Vector2( get_pos().x - (throw_speed * delta), get_pos().y ) )
	
	if get_pos().x > 340 or get_pos().x < -10: # out of screeen
		get_parent().set_game_over()
		queue_free()
#		get_parent().set_game_over()

