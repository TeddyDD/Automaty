
extends Node2D


var animation # animation player
var sound # sample player

var attack = false
var died = false

signal on_hit(type) # sent to enemies

func _ready():
	set_process( true )
	animation = get_node("AnimationPlayer")
	animation.set_current_animation("idle")
	sound = get_node("SamplePlayer2D")
	
func _process(delta):
	if Input.is_action_pressed("ui_cancel") and not attack:
		animation.play("kick")
		sound.play("kick")
		attack = true
		emit_signal("on_hit", "kick")
	elif Input.is_action_pressed("ui_accept") and not attack:
		animation.play("hit")
		sound.play("hit")
		attack = true
		emit_signal("on_hit", "hit")
	elif not attack:
		animation.play("idle")
	prints(attack, animation.get_current_animation(), animation.get_current_animation_pos())

func reset_attack(type): # called from animation
	var t = get_node("cooldown")
	t.stop()
	if type == "hit":
		animation.play("idle")
		t.set_wait_time(0.4)
		t.start()
	elif type == "kick":
		animation.play("idle")
		t.set_wait_time(1.2)
		t.start()

func _on_cooldown_timeout():
	attack = false


func kill():
	set_process( false )
	died = true
	animation.play("die")
