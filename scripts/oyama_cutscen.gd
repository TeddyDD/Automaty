
extends TextureFrame

var lock = true

func _ready():
	set_process(true)
	
func next():
	get_tree().change_scene("res://scenes/karateka/oyama_tutorial.tscn")

func set_pic(n):
	get_node("pic_anim").play("intro" + str(n) )

func _process(delta):
	if Input.is_action_pressed("ui_accept") and not lock:
		next()

func _on_lock_timeout():
	lock = false
