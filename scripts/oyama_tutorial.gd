
extends TextureFrame

var lock = true

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	var skip = Input.is_action_pressed("ui_accept")
	if skip and not lock:
		next_scene()

func _on_lock_timeout():
	lock = false

func next_scene():
	music.get_node("oyama_title").stop()
	music.get_node("oyama").play()
	get_tree().change_scene("res://scenes/karateka.xml")