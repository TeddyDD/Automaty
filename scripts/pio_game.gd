
extends Node2D

var points
var timer
var time = 0

var game_over = false

func _ready():
	global.score = 0
	music.stop_all()
	music.play("pio")
	set_fixed_process( true )
	
	points = get_node("Points")
	timer = get_node("Time")
	
func _fixed_process(delta):
	points.set_text("Points: " + str(global.score))
	timer.set_text("Time: " + str(time))
	
	if game_over:
		get_node("game_over").show()
		get_node("bilocation_txt").hide()
		get_node("bilocation_timer").stop()
		get_node("bilocation_every").stop()
		get_node("fight").stop()
		
		if Input.is_action_pressed("ui_cancel"):
			global.game = "pio"
			get_tree().change_scene("res://scenes/hiscore.tscn")


func _on_counter_timeout():
	time += 1


func _on_bilocation_every_timeout():
	if not game_over:
		get_node("AnimationPlayer").play("bilocation_on")
		get_node("bilocation_every").stop()
		get_node("bilocation_timer").start()


func _on_bilocation_timer_timeout():
	get_node("AnimationPlayer").play("bilocation_off")
	get_node("bilocation_every").start()
