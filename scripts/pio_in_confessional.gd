
extends Node2D

var direction = "right"
signal swap(side)

func _ready():
	set_fixed_process( true )\
	
func _fixed_process(delta):
	var left = Input.is_action_pressed("ui_left")
	var rigt = Input.is_action_pressed("ui_right")
	var new_side
	
	if not get_parent().get_parent().game_over:
		if left:
			new_side = "left"
		elif rigt:
			new_side = "right"
		else:
			new_side = "center"
			
		
	if not new_side == direction:
		direction = new_side
		emit_signal( "swap", direction )
	
	if direction == "left":
		get_node("Sprite").set_flip_h( true )
		get_node("Sprite").show()
		get_node("cener").hide()
	elif direction == "right":
		get_node("Sprite").show()
		get_node("cener").hide()
		get_node("Sprite").set_flip_h( false )

	elif direction == "center":
		get_node("Sprite").hide()
		get_node("cener").show()
