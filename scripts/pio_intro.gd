
extends Node2D

var lock = true

func _ready():
	music.stop_all()
	music.play("pio_intro")
	set_process( true )

func _process(delta):
	if not lock:
		if Input.is_action_pressed("ui_cancel") or Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left"):
			get_node("AnimationPlayer").play("fade")
		
func switch_scene():
	get_tree().change_scene( "res://scenes/pio/pio_anim.tscn" )
	

func _on_Timer_timeout():
	lock = false