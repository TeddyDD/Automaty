
extends TextureFrame

var lock = true

func _ready():
	set_process(true)
	
func _process(delta):
	if Input.is_action_pressed("ui_cancel") and not lock:
		next_scene()

func next_scene():
	get_tree().change_scene("res://scenes/pio/pio_loading.tscn")

func _on_unlock_timeout():
	lock = false