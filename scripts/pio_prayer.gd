
extends Area2D

# state
var direction = "left" setget direction_set
var waiting_for_confession = false
var confessed = false
var confessing = false
var game_over = false
var stop = false

# vars
onready var bar = get_node("progress")
onready var level = get_parent()
onready var animation = get_node("AnimationPlayer")

var speed = 20
var add_at_wait = 15
const optimal_wait = 50

# assets
var textures = [
	preload("res://textures/pio-g1.png"),
	preload("res://textures/pio-w2.png"),
	preload("res://textures/pio-biedron.png"),
	preload("res://textures/pio-w1.png")
]

func _ready():
	# random stuff
#	speed += (randi() % 10) - 5
	add_at_wait += (randi() % 10) - 5
	bar.set_value(0)
	set_fixed_process( true )
	
	# texture
	get_node("Sprite").set_texture(textures[randi() % textures.size()])
	
	# signal
	get_node("../pio_in_confessional").connect("swap", self, "on_pio_swap")

# flip
func direction_set(newvalue):
	assert(newvalue == "left" or newvalue == "right")
	if newvalue == "right":
		set_scale( Vector2(-1,1) )
	else:
		set_scale( Vector2(1,1) )
	direction = newvalue
	

func _fixed_process(delta):
	
	var current_animation = animation.get_current_animation()
	var new_animation =  current_animation
	
	var motion = Vector2()
	if not stop and not confessed and not confessing and not waiting_for_confession and not level.get_parent().game_over:
		if direction == "right":
			motion = Vector2(speed,0)
		else:
			motion = Vector2(-speed, 0)
		set_pos(get_pos() + (motion * delta))
		new_animation = "move"
	
	if confessing and not waiting_for_confession and not level.get_parent().game_over:
		if direction == "right":
			set_pos( Vector2(144, 49 ) )
		else:
			set_pos( Vector2(187, 49 ) )
		new_animation = "confess"
		bar.set_value( bar.get_value() + add_at_wait * delta )
		if bar.get_value() >= optimal_wait:
			confessed = true
		if bar.get_value() == 100:
			level.get_parent().game_over = true
	
	# animations
	if stop:
		new_animation = "idle"
	if level.get_parent().game_over:
		new_animation = "idle"
		stop = true
	if waiting_for_confession:
		new_animation = "idle"
	
	if not new_animation == current_animation:
		animation.play(new_animation)
		
	
	
func on_pio_swap(side):
	if direction == side or side == "center": # turn away
		if confessed:
			global.score += 10
			get_parent().conf = false
			if direction == "right":
				level.left -= 1
			else:
				level.right -= 1
			queue_free()
		elif confessing:
			level.get_parent().game_over = true
	else:
		if bar.get_value() > optimal_wait:
			level.get_parent().game_over = true


func start_confession():
#	prints("start confessin")
	confessing = true
	waiting_for_confession = false
	stop = false
	bar.show()
	remove_from_group( "waiting" )

# collision with other player or with confesionall
func _on_prayer_area_enter( area ):
	if area extends get_script(): # is enemy?
		if direction == "right" and get_pos().x < area.get_pos().x: # I'm second in queue so I have to wait
			stop = true
		elif direction == "left" and get_pos().x > area.get_pos().x:
			stop = true
	else: # confessional
#		prints("waiting for confession")
		waiting_for_confession = true # i have to wait
		add_to_group( "waiting" )


func _on_prayer_area_exit( area ):
	if area extends get_script() and stop: # I'll stay here for a little bit and I can go
		get_node("stop_timer").start()


func _on_stop_timer_timeout():
	stop = false
