
extends Area2D

var speed = 120
var move = false


func _ready():
	set_fixed_process(true)
	
func _fixed_process(delta):
	# get_parent().play and not get_tree().get_current_scene().game_over:
	if get_parent().get_parent().play and not get_tree().get_current_scene().game_over:
		var up = Input.is_action_pressed("ui_up") # temporary
		var down = Input.is_action_pressed("ui_down")
		var was_moving = move
		
		if up or down:
			move = true
			var motion = Vector2()
			if up and get_pos().y > 26:
				motion.y = -speed * delta
			if down and get_pos().y < 98:
				motion.y = speed * delta
			set_pos( get_pos() + motion )
		else:
			move = false
		
		if was_moving != move:
			if move:
				get_node("AnimationPlayer").play("move")
			else:
				get_node("AnimationPlayer").play("idle")
				
			
func _on_pio_area_enter( area ):
	get_tree().get_current_scene().game_over = true
